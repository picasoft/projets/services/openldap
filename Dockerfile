FROM osixia/openldap:1.5.0
LABEL maintainer="quentinduchemin@tuta.io,bonnest@utc.fr"

RUN apt-get update && \
    apt-get install -y rsyslog logrotate

# TODO ajouter un cron
COPY ./logrotate/openldap /etc/logrotate.d/openldap

RUN update-rc.d rsyslog defaults

ADD bootstrap /container/service/slapd/assets/config/bootstrap
ADD environment /container/environment/01-custom
